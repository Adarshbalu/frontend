import 'package:flutter/material.dart';

class AppAlert {
  static showAlertDialog(
      {@required BuildContext context,
      Widget cancelButton = const SizedBox(),
      @required Widget continueButton,
      @required String title,
      @required String subtitle}) {
    AlertDialog alert = AlertDialog(
      title: Text(title),
      content: Text(subtitle),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
