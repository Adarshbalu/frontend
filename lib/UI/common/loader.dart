import 'package:Sumedha/utilities/constants/string_constants.dart';
import 'package:flutter/material.dart';

class Loader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          SizedBox(
            height: 100,
            child: Image.asset(
              LogoStringConstants.logo,
              fit: BoxFit.fitHeight,
            ),
          ),
          SizedBox(
            height: 8,
          ),
          CircularProgressIndicator(),
        ],
      ),
    );
  }
}
