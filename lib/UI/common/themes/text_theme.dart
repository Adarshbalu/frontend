import 'package:Sumedha/UI/common/themes/color_constants.dart';
import 'package:flutter/material.dart';

class TextThemes {
  // Light TextTheme
  static TextTheme lightTextTheme = ThemeData.light().textTheme.copyWith(
        headline1: ThemeData.light().textTheme.headline1.copyWith(
              fontWeight: FontWeight.w400,
            ),
        headline2: ThemeData.light().textTheme.headline2.copyWith(
              fontWeight: FontWeight.w400,
            ),
        headline3: ThemeData.light().textTheme.headline3.copyWith(
            fontWeight: FontWeight.w400, color: ColorConstants.primaryBlack),
        headline4: ThemeData.light().textTheme.headline4.copyWith(
            fontWeight: FontWeight.w400, color: ColorConstants.primaryBlack),
        headline5: ThemeData.light().textTheme.headline5.copyWith(
              fontWeight: FontWeight.w400,
            ),
        headline6: ThemeData.light().textTheme.headline6.copyWith(
              fontWeight: FontWeight.w400,
            ),
        bodyText1: ThemeData.light().textTheme.bodyText1.copyWith(
              fontWeight: FontWeight.w400,
            ),
        bodyText2: ThemeData.light().textTheme.bodyText2.copyWith(
              fontWeight: FontWeight.w400,
            ),
        subtitle1: ThemeData.light().textTheme.subtitle1.copyWith(
              fontWeight: FontWeight.w400,
            ),
        subtitle2: ThemeData.light().textTheme.subtitle2.copyWith(
              fontWeight: FontWeight.w400,
            ),
        caption: ThemeData.light().textTheme.caption.copyWith(
              fontWeight: FontWeight.w400,
            ),
      );

// Light Primary TextTheme

  static TextTheme lightPrimaryTextTheme =
      ThemeData.light().primaryTextTheme.copyWith(
            headline1: ThemeData.light().primaryTextTheme.headline1.copyWith(
                  fontWeight: FontWeight.w400,
                ),
            headline2: ThemeData.light().primaryTextTheme.headline2.copyWith(
                  fontWeight: FontWeight.w400,
                ),
            headline3: ThemeData.light().primaryTextTheme.headline3.copyWith(
                  fontWeight: FontWeight.w400,
                ),
            headline4: ThemeData.light().primaryTextTheme.headline4.copyWith(
                  fontWeight: FontWeight.w400,
                ),
            headline5: ThemeData.light().primaryTextTheme.headline5.copyWith(
                  fontWeight: FontWeight.w400,
                ),
            headline6: ThemeData.light().primaryTextTheme.headline6.copyWith(
                  fontWeight: FontWeight.w400,
                ),
            bodyText1: ThemeData.light().primaryTextTheme.bodyText1.copyWith(
                  fontWeight: FontWeight.w400,
                ),
            bodyText2: ThemeData.light().primaryTextTheme.bodyText2.copyWith(
                  fontWeight: FontWeight.w400,
                ),
            subtitle1: ThemeData.light().primaryTextTheme.subtitle1.copyWith(
                  fontWeight: FontWeight.w400,
                ),
            subtitle2: ThemeData.light().primaryTextTheme.subtitle2.copyWith(
                  fontWeight: FontWeight.w400,
                ),
            caption: ThemeData.light().primaryTextTheme.caption.copyWith(
                  fontWeight: FontWeight.w400,
                ),
          );

//Dark text theme

  static TextTheme darkTextTheme = ThemeData.dark().textTheme.copyWith(
        headline1: ThemeData.dark()
            .textTheme
            .headline1
            .copyWith(fontWeight: FontWeight.w400, color: Colors.white),
        headline2: ThemeData.dark()
            .textTheme
            .headline2
            .copyWith(fontWeight: FontWeight.w400, color: Colors.white),
        headline3: ThemeData.dark()
            .textTheme
            .headline3
            .copyWith(fontWeight: FontWeight.w400, color: Colors.white),
        headline4: ThemeData.dark()
            .textTheme
            .headline4
            .copyWith(fontWeight: FontWeight.w400, color: Colors.white),
        headline5: ThemeData.dark()
            .textTheme
            .headline5
            .copyWith(fontWeight: FontWeight.w400, color: Colors.white),
        headline6: ThemeData.dark()
            .textTheme
            .headline6
            .copyWith(fontWeight: FontWeight.w400, color: Colors.white),
        bodyText1: ThemeData.dark()
            .textTheme
            .bodyText1
            .copyWith(fontWeight: FontWeight.w400, color: Colors.white),
        bodyText2: ThemeData.dark()
            .textTheme
            .bodyText2
            .copyWith(fontWeight: FontWeight.w400, color: Colors.white),
        subtitle1: ThemeData.dark()
            .textTheme
            .subtitle1
            .copyWith(fontWeight: FontWeight.w400, color: Colors.white),
        subtitle2: ThemeData.dark()
            .textTheme
            .subtitle2
            .copyWith(fontWeight: FontWeight.w400, color: Colors.white),
        caption: ThemeData.dark().textTheme.caption.copyWith(
              fontWeight: FontWeight.w400,
            ),
      );

// Light Primary TextTheme

  static TextTheme darkPrimaryTextTheme =
      ThemeData.dark().primaryTextTheme.copyWith(
            headline1: ThemeData.dark().primaryTextTheme.headline1.copyWith(
                fontWeight: FontWeight.w400,
                color: ColorConstants.primaryBlack),
            headline2: ThemeData.dark().primaryTextTheme.headline2.copyWith(
                fontWeight: FontWeight.w400,
                color: ColorConstants.primaryBlack),
            headline3: ThemeData.dark().primaryTextTheme.headline3.copyWith(
                fontWeight: FontWeight.w400,
                color: ColorConstants.primaryBlack),
            headline4: ThemeData.dark().primaryTextTheme.headline4.copyWith(
                fontWeight: FontWeight.w400,
                color: ColorConstants.primaryBlack),
            headline5: ThemeData.dark().primaryTextTheme.headline5.copyWith(
                fontWeight: FontWeight.w400,
                color: ColorConstants.primaryBlack),
            headline6: ThemeData.dark().primaryTextTheme.headline6.copyWith(
                fontWeight: FontWeight.w400,
                color: ColorConstants.primaryBlack),
            bodyText1: ThemeData.dark().primaryTextTheme.bodyText1.copyWith(
                fontWeight: FontWeight.w400,
                color: ColorConstants.primaryBlack),
            bodyText2: ThemeData.dark().primaryTextTheme.bodyText2.copyWith(
                fontWeight: FontWeight.w400,
                color: ColorConstants.primaryBlack),
            subtitle1: ThemeData.dark().primaryTextTheme.subtitle1.copyWith(
                fontWeight: FontWeight.w400,
                color: ColorConstants.primaryBlack),
            subtitle2: ThemeData.dark().primaryTextTheme.subtitle2.copyWith(
                fontWeight: FontWeight.w400,
                color: ColorConstants.primaryBlack),
            caption: ThemeData.dark().primaryTextTheme.caption.copyWith(
                fontWeight: FontWeight.w400,
                color: ColorConstants.primaryBlack),
            button: ThemeData.dark()
                .primaryTextTheme
                .button
                .copyWith(color: ColorConstants.primaryBlack),
          );
}
