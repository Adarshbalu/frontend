import 'package:Sumedha/providers/questions_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SumedhaAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  final Size preferredSize;
  final bool ignoreTouches;
  SumedhaAppBar({this.ignoreTouches = false})
      : preferredSize = Size.fromHeight(56.0);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 8),
        child: AppBar(
          elevation: 0,
          centerTitle: true,
          title: Text(
            'Sumedha',
            style: Theme.of(context).textTheme.headline5,
          ),
          // actions: <Widget>[
          //   IconButton(
          //     icon: Icon(Icons.search),
          //     onPressed: () {
          //       if (!ignoreTouches)
          //         showSearch(
          //           context: context,
          //           delegate: CustomSearchBar(),
          //         );
          //     },
          //   ),
          // ],
        ));
  }
}

class ExamAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  final Size preferredSize;

  ExamAppBar() : preferredSize = Size.fromHeight(56.0);

  @override
  Widget build(BuildContext context) {
    final QuestionsProvider questionsProvider =Provider.of(context);
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 8),
        child: AppBar(
          elevation: 0,
          centerTitle: true,
          title: Text(
            'Sumedha',
            style: Theme.of(context).textTheme.headline5,
          ),
          leading: IconButton(
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
              icon: Icon(
                Icons.info_outline_rounded,
                size: 35,
                color: Theme.of(context).accentColor,
              )),
          actions: [
            IconButton(icon: Icon(Icons.done,
            size: 35,
              color: Colors.green,
            ),
            onPressed: (){
              if(questionsProvider.currentQuestion <=
                  questionsProvider.totalQuestions)

            questionsProvider.gotoLastQuestion();
            },
            ),
          ],
        ));
  }
}
