import 'package:Sumedha/UI/common/themes/color_constants.dart';
import 'package:Sumedha/UI/screens/dashboard/card_widgets.dart';
import 'package:Sumedha/UI/screens/news/news_list.dart';
import 'package:Sumedha/models/progress.dart';
import 'package:Sumedha/providers/progress_provider.dart';
import 'package:Sumedha/utilities/screen_size.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class DashboardCards extends StatefulWidget {
  @override
  _DashboardCardsState createState() => _DashboardCardsState();
}

class _DashboardCardsState extends State<DashboardCards> {
  ScreenSize screenSize;

  @override
  Widget build(BuildContext context) {
    screenSize = ScreenSize(context);

    return Scaffold(
      body: Consumer<ProgressProvider>(
        builder: (context, progress, child) => FutureBuilder<ProgressModel>(
            future: progress.getProgress(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.all(8),
                        child: Image.asset('assets/images/logo.png'),
                        height: screenSize.height / 5,
                      ),
                      LargeCard(
                        progress: snapshot.data,
                      ),
                      GestureDetector(
                        onTap: () async {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => NewsListHome(),
                            ),
                          );
                        },
                        child: SmallCard(
                          subtitle: 'Here\'s top ten headlines for the day',
                          title: 'Daily News Headlines',
                          icon: FontAwesomeIcons.globeAmericas,
                          color: ColorConstants.deepBlue,
                        ),
                      ),
                      // SmallCard(
                      //   screenSize: screenSize,
                      //   icon: FontAwesomeIcons.tasks,
                      //   title: 'Assignment due today',
                      //   subtitle: 'Go to Schedule',
                      //   color: ColorConstants.deepOrange,
                      // ),
                    ],
                  ),
                );
              }
              return Center(child: CircularProgressIndicator());
            }),
      ),
    );
  }
}
