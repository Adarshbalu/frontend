import 'package:flutter/material.dart';

class UserDetails extends StatelessWidget {
  final String name;
  final String userName;
  UserDetails({
    this.name,
    this.userName,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 10,
          ),
          Center(
            child: CircleAvatar(
              child: Icon(
                Icons.person,
                size: 50,
              ),
              radius: 60.0,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Center(
            child: Text(
              this.userName,
              style: Theme.of(context).textTheme.headline6,
            ),
          ),
          Center(
            child: Text(
              this.name,
              style: Theme.of(context).textTheme.headline4,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Center(
              child: SizedBox(
            child: Divider(
              thickness: 1.3,
            ),
            width: 240,
          )),
          SizedBox(
            height: 10,
          ),
          Container(
              padding: EdgeInsets.only(left: 10),
              child: Text(
                "Previous Test Score",
                style: Theme.of(context).textTheme.headline6,
              )),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }
}
