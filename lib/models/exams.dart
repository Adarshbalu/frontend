// To parse this JSON data, do
//
//     final exam = examFromMap(jsonString);

import 'dart:convert';

List<Exam> examFromMap(String str) =>
    List<Exam>.from(json.decode(str).map((x) => Exam.fromMap(x)));

String examToMap(List<Exam> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

class Exam {
  Exam({
    this.id,
    this.name,
    this.description,
    this.file,
    this.slug,
    this.duration,
    this.questionsCount,
  });

  int id;
  String name;
  String description;
  String file;
  String slug;
  int questionsCount;
  int duration;

  Exam copyWith({
    int id,
    String name,
    String description,
    String file,
    String slug,
    int questionsCount,
    int duration,
  }) =>
      Exam(
        id: id ?? this.id,
        name: name ?? this.name,
        description: description ?? this.description,
        file: file ?? this.file,
        slug: slug ?? this.slug,
        duration: duration ?? this.duration,
        questionsCount: questionsCount ?? this.questionsCount,
      );

  factory Exam.fromMap(Map<String, dynamic> json) => Exam(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        file: json["file"],
        slug: json["slug"],
        duration: json["time_limit"],
        questionsCount: json["questions_count"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "description": description,
        "file": file,
        "slug": slug,
        "questions_count": questionsCount,
        "time_limit": duration,
      };
}
