import 'dart:convert';

List<ProfileData> profileDatafromMap(String str) =>
    List<ProfileData>.from(json.decode(str).map((x) => ProfileData.fromMap(x)));

// String studentExamToMap(List<StudentExam> data) =>
//     json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

class ProfileData {
  ProfileData({
    this.username,
    this.email,
    this.name,
  });

  String username;
  String email;
  String name;

  ProfileData copyWith({
    String username,
    String email,
    String name,
  }) =>
      ProfileData(
        username: username ?? this.username,
        name: name ?? this.name,
        email: email ?? this.email,
      );

  factory ProfileData.fromMap(Map<String, dynamic> json) => ProfileData(
        name: json["name"],
        email: json["email"],
        username: json["username"],
      );

  Map<String, dynamic> toMap() => {
        "name": name,
        "email": email,
        "username": username,
      };
}
