import 'package:Sumedha/models/profile_data.dart';
import 'package:Sumedha/models/student_exam.dart';
import 'package:Sumedha/services/api_service.dart';
import 'package:Sumedha/utilities/constants/string_constants.dart';
import 'package:Sumedha/utilities/storage_helper.dart';
import 'package:flutter/widgets.dart';

class ProfileProvider extends ChangeNotifier {
  DataSource _dataSource = DataSource();
  List<StudentExam> studentExam;
  List<ProfileData> profileData;

  StorageHelper _storageHelper = StorageHelper();
  Future<List<StudentExam>> getProfileData() async {
    if (studentExam == null) {
      await getNewAccessToken();
      String access =
          await _storageHelper.getString(AuthStringConstants.access);
      this.studentExam = await _dataSource.getStudentExams(access: access);
      notifyListeners();
    }
    return this.studentExam;
  }

  Future<List<ProfileData>> getUserName() async {
    if (profileData == null) {
      await getNewAccessToken();
      String access =
          await _storageHelper.getString(AuthStringConstants.access);
      this.profileData = await _dataSource.getProfileDatas(access: access);

      notifyListeners();
    }
    return this.profileData;
  }

  Future<void> getNewAccessToken() async {
    String refresh =
        await _storageHelper.getString(AuthStringConstants.refresh);
    await _dataSource.refresh(refresh).then((value) async => {
          await _storageHelper.saveString(
              key: AuthStringConstants.access, value: value.access)
        });
  }

  void refresh() {
    this.profileData = null;
    this.studentExam = null;
    notifyListeners();
  }
}
